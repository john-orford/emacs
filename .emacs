;; Package Initialization
(require 'package)
(setq package-archives '(("melpa-stable" . "https://stable.melpa.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("gnu" . "https://elpa.gnu.org/packages/")))
(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))

;; Install 'use-package'
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;; Basic Settings
(global-unset-key (kbd "C-z"))
;; (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3") ;; Removed due to potential issues
(setq-default indent-tabs-mode nil)
(desktop-save-mode 1)
(savehist-mode 1)
(add-to-list 'savehist-additional-variables 'kill-ring)
(show-paren-mode t)

;; Proxy Settings
(setq url-proxy-services
      '(("no_proxy" . "^\\(localhost\\|10\\..*\\|192\\.168\\..*\\)")
        ("http" . "de.coia.siemens.net:9400")
        ("https" . "de.coia.siemens.net:9400")))

;; Ensure Dependencies are Installed
(use-package spinner
  :ensure t)

(use-package seq
  :ensure t)

;; Rust Configuration
(use-package rust-mode
  :mode "\\.rs\\'"
  :config
  (setq rust-format-on-save t))

(use-package lsp-mode
  :hook ((rust-mode . lsp)
         (elm-mode . lsp)
         (haskell-mode . lsp)
         (haskell-literate-mode . lsp))
  :commands lsp
  :config
  (setq lsp-rust-server 'rust-analyzer
        lsp-log-io t))

(use-package lsp-ui
  :commands lsp-ui-mode
  :config
  (setq lsp-ui-doc-enable t
        lsp-ui-doc-show-with-cursor t
        lsp-ui-doc-position 'at-point
        lsp-ui-doc-include-signature t))

(global-set-key (kbd "C-c a") 'lsp-execute-code-action)

;; Flycheck Configuration
(use-package flycheck
  :init (global-flycheck-mode))

;; Company Configuration
(use-package company
  :hook (after-init . global-company-mode)
  :config
  (setq company-minimum-prefix-length 1
        company-idle-delay 0.0))

;; Cargo Integration
(use-package cargo
  :ensure t)

;; LSP Haskell
(use-package lsp-haskell
  :ensure t)

;; ORMOLU for Haskell
(use-package ormolu
  :hook (haskell-mode . ormolu-format-on-save-mode)
  :bind
  (:map haskell-mode-map
        ("C-c r" . ormolu-format-buffer)))

;; Elm Mode Configuration
(use-package elm-mode
  :mode "\\.elm\\'"
  :config
  (add-hook 'elm-mode-hook 'elm-format-on-save-mode)
  ;; Error checking
  (use-package flycheck-elm
    :config
    (add-hook 'flycheck-mode-hook #'flycheck-elm-setup))
  ;; Adjustments for better experience
  (setq elm-tags-on-save t
        elm-sort-imports-on-save t))

;; YAML Mode Configuration
(use-package yaml-mode
  :ensure t
  :mode ("\\.yml\\'" "\\.yaml\\'")
  :config
  (add-hook 'yaml-mode-hook
            (lambda ()
              (define-key yaml-mode-map (kbd "C-m") 'newline-and-indent)
              (highlight-lines-matching-regexp "^\s*-\s*\\w+:")
              (highlight-lines-matching-regexp "^\s*\s*\\w+:")
              (company-mode)))
  (setq yaml-indent-offset 2))

;; Bitbake Mode Configuration
(use-package bitbake
  :ensure t
  :mode (("\\.bb\\'" . bitbake-mode)
         ("\\.bbappend\\'" . bitbake-mode)
         ("\\.bbclass\\'" . bitbake-mode))
  :config
  (add-hook 'bitbake-mode-hook 'flycheck-mode)
  (add-hook 'bitbake-mode-hook 'company-mode))

;; Blacken for Python
(use-package blacken
  :hook (python-mode . blacken-mode)
  :config
  (setq blacken-line-length 79)
  (add-hook 'before-save-hook 'blacken-buffer t t))


(use-package nix-mode
  :mode "\\.nix\\'"
  :hook ((nix-mode . lsp-deferred)
         (nix-mode . flycheck-mode)
         (nix-mode . company-mode)))

;; Additional Packages and Configurations
;; ...

;; Which-Key
(use-package which-key
  :config
  (which-key-mode))

;; Set Font
(set-frame-font "Menlo-12" nil t)

;; All The Icons
(use-package all-the-icons
  :ensure t)

;; Dashboard
(use-package dashboard
  :config
  (dashboard-setup-startup-hook))

;; OSC 52 Clipboard Support
(defun osc52-send-region-to-clipboard (start end)
  (interactive "r")
  (let* ((text (buffer-substring-no-properties start end))
         (encoded-text (base64-encode-string (encode-coding-string text 'utf-8) t))
         (osc-cmd (concat "\e]52;c;" encoded-text "\a")))
    (send-string-to-terminal osc-cmd)))

(global-set-key (kbd "C-c C-c") 'osc52-send-region-to-clipboard)

;; Optional: Override the default copy command
(setq interprogram-cut-function 
      (lambda (text &optional push)
        (osc52-send-region-to-clipboard (point) (mark))))

;; Clipboard Settings
(setq x-select-enable-clipboard t
      x-select-enable-primary t)

;; Mouse Support
(xterm-mouse-mode 1)

;; Enable mouse wheel support (optional)
(defun scroll-up-with-lines () 
  "Scroll up 4 lines."
  (interactive) 
  (scroll-up 4))

(defun scroll-down-with-lines () 
  "Scroll down 4 lines."
  (interactive) 
  (scroll-down 4))

(global-set-key (kbd "<mouse-4>") 'scroll-down-with-lines)
(global-set-key (kbd "<mouse-5>") 'scroll-up-with-lines)

;; Indentation Functions
(defun my-indent-region (n)
  "Indent the region, or if no region is active, indent the current line."
  (interactive "p")
  (if (use-region-p)
      (let ((start (region-beginning))
            (end (region-end)))
        (indent-rigidly start end n)
        (setq deactivate-mark nil)
        (set-mark start)
        (goto-char end))
    (indent-according-to-mode)))

(defun my-unindent-region (n)
  "Unindent the region, or if no region is active, unindent the current line."
  (interactive "p")
  (if (use-region-p)
      (let ((start (region-beginning))
            (end (region-end)))
        (indent-rigidly start end (- n))
        (setq deactivate-mark nil)
        (set-mark start)
        (goto-char end))
    (let ((current-point (point)))
      (beginning-of-line)
      (backward-delete-char-untabify (max 1 (* n tab-width)))
      (goto-char current-point))))

(defun setup-my-indentation-keys ()
  (local-set-key [tab] 'my-indent-region)
  (local-set-key [backtab] 'my-unindent-region))

(add-hook 'text-mode-hook 'setup-my-indentation-keys)
(add-hook 'prog-mode-hook 'setup-my-indentation-keys)



;; Custom Set Variables and Faces
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(adwaita))
 '(custom-safe-themes
   '("1d2e7f3afdd436cf7b1f7d009111e9890328da1f68380c71ad8041ebd62a0a95" "c0f4b66aa26aa3fded1cbefe50184a08f5132756523b640f68f3e54fd5f584bd" "e8830baf7d8757f15d9d02f9f91e0a9c4732f63c3f7f16439cc4fb42a1f2aa06" default))
 '(package-selected-packages
   '(yaml-mode which-key bitbake rust-mode dockerfile-mode blacken elm-format elm-oracle flycheck-elm org-bullets dashboard all-the-icons doom-modeline ormolu flycheck lsp-mode yasnippet company nix-repl nix-shell nix-drv-mode lsp-mode nix-mode lsp-ui lsp-haskell elm-mode)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

